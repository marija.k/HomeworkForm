<?php
namespace App\Http\Controllers;
use \Illuminate\Http\Request;
require '../vendor/autoload.php';

use Carbon\Carbon;

class KlientController extends Controller
{
	public function prikazForma()
	{
		return view('form_klient');
	}
	
	public function prikazPodatoci(Request $request)
	{
		$ime=$request->get('ime');
		$prezime=$request->get('prezime');
		$pol=$request->get('pol');
		$datum=$request->get('datum');
		$datumArr=explode('.', $datum);
		$age=Carbon::createFromDate($datumArr[2], $datumArr[1], $datumArr[0])->diff(Carbon::now())->format('%y години');

		return view('podatoci_korisnik',[
			'ime' => $ime,
			'prezime' => $prezime,
			'pol' => $pol,
			'age' => $age,
			]);
	}
}