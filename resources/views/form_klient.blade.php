<h1>Клиент:</h1>
<form action="{{ route('podatoci_korisnik') }}" method="post">
	
	{{ csrf_field() }}

	Име: <input type="text" name="ime"/><br/>
	Презиме: <input type="text" name="prezime"/><br/>
	Пол: <select name="pol">
		<option value="Машки">Машки</option>
		<option value="Женски">Женски</option>		
	</select><br/>
	Датум на раѓање: (формат: ден.месец.година) <input type="text" name="datum" pattern="\d{1,2}\.\d{1,2}\.\d{4}" title="Пр: 23.08.1995"><br/>
	<input type="submit" value="Испрати">
</form>
